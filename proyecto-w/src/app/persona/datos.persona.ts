export class DatosPersona{
    public id :number = 0;
    public nombre : string ='';
    public apellidos : string = '';
    public telefono : number ;
    public correo : string = '';
    public direccion : string = '';
}