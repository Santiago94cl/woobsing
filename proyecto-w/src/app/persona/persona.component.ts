import { Component, OnInit } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { DatosPersona } from './datos.persona';


@Component({
  selector: 'app-persona',
  templateUrl: './persona.component.html',
  styleUrls: ['./persona.component.css']
})
export class PersonaComponent implements OnInit {

  personas : DatosPersona[] = [

    {"id":1,"nombre":"santiago","apellidos":"cubillos","telefono":3872583,"correo":"santiago@gmail.com","direccion":"cll 86"},
    {"id":2,"nombre":"camilo","apellidos":"castro","telefono":3326583,"correo":"camilo@gmail.com","direccion":"cll 90"},

  ]
  
  datosNuevos : DatosPersona = new DatosPersona();

  constructor() { }

  ngOnInit(): void {
  }

  eliminarPersona(item:any){
    this.personas = this.personas.filter(x => x != item);
    this.datosNuevos  = new DatosPersona();
  }
  agregarPersona(){
    this.datosNuevos.id = this.personas.length +1;
    this.personas.push(this.datosNuevos);
    this.datosNuevos  = new DatosPersona();
  }

}
